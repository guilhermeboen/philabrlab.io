---
title: "Elenco"
date: 2022-10-08T12:31:54-03:00
draft: False
---
Técnico: Doc Rivers


Armadores:
* James Harden - Número 1

* Tyrese Maxey - Número 0

* Charlie Brown Jr - Número 16

* Matisse Thybulle - Número 22

* Isaiah Joe - Número 7

* Furkan Korkmaz - Número 30

* Shake MiltoN - Número 18

Alas:
* Tobias Harris - Número 12

* Danuel House Jr - Número 25

* Paul Reed - Número 44

* P.J. Tucker - Número 17

* Georges Niang - Número 20

Pivôs:
* Joel Embiid - Número 21

* Montrezl Harrell - Número 28

* Charles Bassey - Número 23