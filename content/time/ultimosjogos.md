---
title: "Comentarios sobre o ultimo jogo"
date: 2022-10-08T11:45:10-03:00
draft: false
---

Neste segundo jogo da pré temporada o sixers começou a partida com força total contra o Cleveland Cavaliers. Com o time titular composto de James Harden, Maxey,Embiid, Harris e o mais novo reforço P. J. Tucker, o Sixers sofreu para organizar a defesa no primeiro quarto, assim sofrendo mais de 30 pontos.

Mas com o passar dos quartos o time foi construindo o entrosamento defensivo e assim diminuindo a intensidade ofensiva dos Cavs. Apartir da virada de quadra, ambos times tiraram o pé do acelerador e colocaram os reservas para jogar. Resultando num final de jogo com nível reduzido, mas ainda com emoção, pois a partida acabou 113-112 para o Philadelphia com destaque para Montrelz Harrell pelo arremesso da vitória.

